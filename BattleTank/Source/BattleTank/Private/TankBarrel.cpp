// Kief 2018

#include "TankBarrel.h"
#include "Engine/World.h"
#include "GameFramework/PlayerController.h"


// Used by TankAimComponent
void UTankBarrel::Elevate(float RelativeSpeed)
{
	float SpeedClamped = FMath::Clamp(RelativeSpeed, -1.f, 1.f);
	float DeltaSpeed = SpeedClamped * MaxDegreesPerSecond * GetWorld()->DeltaTimeSeconds;
	float RawElevation = RelativeRotation.Pitch + DeltaSpeed;
	float ElevationClamped = FMath::Clamp(RawElevation, MinElevationDegrees, MaxElevationDegrees);

	SetRelativeRotation(FRotator(ElevationClamped, 0, 0));
}
