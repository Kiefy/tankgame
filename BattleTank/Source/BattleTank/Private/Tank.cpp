// Kief 2018

#include "Tank.h"

#include "GameFramework/Controller.h"
#include "AIController.h"


// Constructor
ATank::ATank()
{
	PrimaryActorTick.bCanEverTick = false;
}


void ATank::BeginPlay()
{
	Super::BeginPlay();

	CurrentHealth = MaxHealth;
}


// Used by WBP_HealthBar Blueprint
float ATank::GetHealthPercent()
{
	return (float)CurrentHealth / (float)MaxHealth;
}


// Override to take damage
float ATank::TakeDamage(float DamageAmount, FDamageEvent const & DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	int32 DamagePoints = FPlatformMath::RoundToInt(DamageAmount);
	int32 DamageToApply = FMath::Clamp(DamagePoints, 0, CurrentHealth);

	CurrentHealth -= DamageToApply;

	if (CurrentHealth <= 0)
	{
		OnDeath.Broadcast();
		OnDeathEvent();
	}

	return CurrentHealth;
}
