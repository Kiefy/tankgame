// Kief 2018

#include "TankAimComponent.h"
#include "TankBarrel.h"
#include "TankTurret.h"
#include "Projectile.h"

#include "Engine/World.h"
#include "GameFramework/Actor.h"
#include "Kismet/GameplayStatics.h"


// Constructor
UTankAimComponent::UTankAimComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}


void UTankAimComponent::BeginPlay()
{
	Super::BeginPlay();

	LastFireTime = GetWorld()->GetTimeSeconds();
}


void UTankAimComponent::TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (AmmoRemaining < 1)
	{
		AimState = EAimState::OutOfAmmo;
	}
	else if ((GetWorld()->GetTimeSeconds() - LastFireTime) < ReloadTimeInSeconds)
	{
		AimState = EAimState::Reloading;
	}
	else if (IsBarrelLocked())
	{
		AimState = EAimState::Locked;
	}
	else
	{
		AimState = EAimState::Aiming;
	}
}


EAimState UTankAimComponent::GetAimState() const { return AimState; }
int32 UTankAimComponent::GetAmmoRemaining() const { return AmmoRemaining; }

// Used by Player/Ai Controllers
void UTankAimComponent::AimAt(FVector TargetLocation)
{
	if (!ensure(Barrel)) { return; }

	FVector LaunchVelocity;
	FVector StartLocation = Barrel->GetSocketLocation(FName("ShootyEnd"));
	bool bHaveAimSolution = UGameplayStatics::SuggestProjectileVelocity(this, OUT LaunchVelocity, StartLocation, TargetLocation, LaunchSpeed, false, 0.f, 0.f, ESuggestProjVelocityTraceOption::DoNotTrace, FCollisionResponseParams::DefaultResponseParam, {}, false);

	if (bHaveAimSolution)
	{
		AimDirection = LaunchVelocity.GetSafeNormal();
		MoveBarrelTowards(AimDirection);
	}
}


// Used in Tank Blueprint
void UTankAimComponent::Initialise(UTankBarrel* BarrelToSet, UTankTurret* TurretToSet)
{
	Barrel = BarrelToSet;
	Turret = TurretToSet;
}


// Used in Tank Input Blueprint
void UTankAimComponent::Fire()
{
	if (AmmoRemaining > 0 && AimState != EAimState::Reloading)
	{
		if (!ensure(Barrel)) { return; }
		FVector ShootLocation = Barrel->GetSocketLocation(FName("ShootyEnd"));
		FRotator ShootRotation = Barrel->GetSocketRotation(FName("ShootyEnd"));

		if (!ensure(ProjectileBlueprint)) { return; }
		auto Projectile = GetWorld()->SpawnActor<AProjectile>(ProjectileBlueprint, ShootLocation, ShootRotation);
		Projectile->Launch(LaunchSpeed);

		LastFireTime = GetWorld()->GetTimeSeconds();
		AmmoRemaining--;
	}
}


// Used by AimAt (above)
void UTankAimComponent::MoveBarrelTowards(FVector AimDirection)
{
	if (!ensure(Barrel) || !ensure(Turret)) { return; }

	FRotator BarrelRotation = Barrel->GetForwardVector().Rotation();
	FRotator AimRotation = AimDirection.Rotation();
	FRotator DeltaRotation = AimRotation - BarrelRotation;

	Barrel->Elevate(DeltaRotation.Pitch);

	if (FMath::Abs(DeltaRotation.Yaw) < 180) { Turret->Rotate(DeltaRotation.Yaw); }
	else { Turret->Rotate(-DeltaRotation.Yaw); }
}


bool UTankAimComponent::IsBarrelLocked()
{
	if (!ensure(Barrel)) { return false; }
	return AimDirection.Equals(Barrel->GetForwardVector(), 0.01);
}
