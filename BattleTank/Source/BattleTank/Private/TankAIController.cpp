// Kief 2018

#include "TankAIController.h"
#include "TankAimComponent.h"
#include "Tank.h"

#include "Engine/World.h"
#include "GameFramework/Actor.h"
#include "GameFramework/Controller.h"
#include "GameFramework/PlayerController.h"


void ATankAIController::BeginPlay()
{
	Super::BeginPlay();
}


void ATankAIController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	auto PlayerTank = GetWorld()->GetFirstPlayerController()->GetPawn();
	auto AITank = GetPawn();

	if (!PlayerTank || !AITank) { return; }
	MoveToActor(PlayerTank, AcceptanceRadius, true, true, false);

	auto AimComponent = AITank->FindComponentByClass<UTankAimComponent>();
	if (!ensureMsgf(AimComponent, TEXT("Player Controller can't find Aiming Component!"))) { return; }

	AimComponent->AimAt(PlayerTank->GetActorLocation());

	if (AimComponent->GetAimState() == EAimState::Locked)
	{
		AimComponent->Fire();
	}
}


void ATankAIController::SetPawn(APawn* InPawn)
{
	Super::SetPawn(InPawn);

	if (InPawn)
	{
		auto PossessedTank = Cast<ATank>(InPawn);
		if (!PossessedTank) { return; }
		PossessedTank->OnDeath.AddUniqueDynamic(this, &ATankAIController::OnDeath);
	}
}


void ATankAIController::OnDeath()
{
	if (!GetPawn()) { return; }
	UE_LOG(LogTemp, Warning, TEXT("AI: %s Destroyed!"), *GetPawn()->GetName());
	GetPawn()->DetachFromControllerPendingDestroy();
}
