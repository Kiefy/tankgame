// Kief 2018

#include "TankTurret.h"
#include "Engine/World.h"


// Used by TankAimComponent
void UTankTurret::Rotate(float RelativeSpeed)
{
	float ClampedSpeed = FMath::Clamp(RelativeSpeed, -1.f, 1.f);
	float RotationChange = ClampedSpeed * MaxDegreesPerSecond * GetWorld()->DeltaTimeSeconds;
	float Rotation = RelativeRotation.Yaw + RotationChange;

	SetRelativeRotation(FRotator(0, Rotation, 0));
}

