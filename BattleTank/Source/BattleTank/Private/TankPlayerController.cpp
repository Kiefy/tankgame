// Kief 2018

#include "TankPlayerController.h"
#include "TankAimComponent.h"
#include "Tank.h"

#include "Engine/World.h"
#include "GameFramework/Actor.h"


void ATankPlayerController::BeginPlay()
{
	Super::BeginPlay();

	auto AimingComponent = GetPawn()->FindComponentByClass<UTankAimComponent>();
	if (!ensureMsgf(AimingComponent, TEXT("Player Controller can't find Aiming Component!"))) { return; }
	FoundAimingComponent(AimingComponent);
}


void ATankPlayerController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	AimThroughCrosshair();
}


void ATankPlayerController::AimThroughCrosshair() const
{
	if (!GetPawn()) { return; }
	auto AimingComponent = GetPawn()->FindComponentByClass<UTankAimComponent>();
	if (!ensureMsgf(AimingComponent, TEXT("Player Controller can't find Aiming Component!"))) { return; }

	FVector AimTargetLocation(ForceInit);

	if (GetSightRayHitLocation(OUT AimTargetLocation))
	{
		AimingComponent->AimAt(AimTargetLocation);
	}
}


// OUT the world location at the Normalized Screen Position (Crosshair)
bool ATankPlayerController::GetSightRayHitLocation(OUT FVector& AimTargetLocation) const
{
	// OUT the view direction vector through the camera/crosshair to pass to AimTrace()
	FVector AimDirection(ForceInit);
	if (!GetAimDirection(OUT AimDirection)) { return false; }

	// OUT the HitResult
	FHitResult HitResult(ForceInit);
	if (!AimTrace(AimDirection, OUT HitResult)) { return false; }

	// Return the OUT parameter
	AimTargetLocation = HitResult.Location;
	return true;
}


// Used by GetSightRayHitLocation (above)
bool ATankPlayerController::GetAimDirection(OUT FVector& AimDirection) const
{
	// Get the Screen/Window size in pixels
	int32 ViewportPixelSizeX, ViewportPixelSizeY;
	GetViewportSize(ViewportPixelSizeX, OUT ViewportPixelSizeY);

	// Get the pixel position of the crosshair
	const FVector2D CrosshairPixelPosition = FVector2D(ViewportPixelSizeX * CrosshairScreenPercentNormal.X, ViewportPixelSizeY * CrosshairScreenPercentNormal.Y);

	FVector CameraWorldLocation; // To be discarded
	return DeprojectScreenPositionToWorld(CrosshairPixelPosition.X, CrosshairPixelPosition.Y, OUT CameraWorldLocation, OUT AimDirection);
}


// Used by GetSightRayHitLocation (above)
bool ATankPlayerController::AimTrace(FVector AimDirection, OUT FHitResult& Hit) const
{
	const FVector TraceStart = PlayerCameraManager->GetCameraLocation();
	const FVector TraceEnd = TraceStart + (AimDirection * AimRange);
	return GetWorld()->LineTraceSingleByChannel(OUT Hit, TraceStart, TraceEnd, ECC_Camera);
}


void ATankPlayerController::SetPawn(APawn* InPawn)
{
	Super::SetPawn(InPawn);

	if (InPawn)
	{
		auto PossessedTank = Cast<ATank>(InPawn);
		if (!PossessedTank) { return; }
		PossessedTank->OnDeath.AddUniqueDynamic(this, &ATankPlayerController::OnDeath);
	}
}


void ATankPlayerController::OnDeath()
{
	if (!GetPawn()) { return; }
	UE_LOG(LogTemp, Warning, TEXT("Player: Tank Destroyed!"));
	StartSpectatingOnly();
}
