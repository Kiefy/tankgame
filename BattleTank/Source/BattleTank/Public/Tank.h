// Kief 2018

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Tank.generated.h"


class UParticleSystemComponent;

// Delegates
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FTankDelegate);

UCLASS()
class BATTLETANK_API ATank : public APawn
{
	GENERATED_BODY()

public:
	// Used by WBP_HealthBar Blueprint
	UFUNCTION(BlueprintPure, Category = "Health")
		float GetHealthPercent();

	// Delegate used by Tank Player/AI Controllers
	FTankDelegate OnDeath;

	UFUNCTION(BlueprintImplementableEvent, Category = "Event")
		void OnDeathEvent();

protected:
	virtual void BeginPlay() override;

private:
	// Constructor
	ATank();

	// Override to take damage
	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, class AActor* DamageCauser) override;

	// Health Variables
	UPROPERTY(EditDefaultsOnly, Category = "Health")
		int32 MaxHealth = 100;

	int32 CurrentHealth;
};
