// Kief 2018

#pragma once

#include "CoreMinimal.h"
#include "Components/SceneComponent.h"
#include "SpawnPoint.generated.h"


UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class BATTLETANK_API USpawnPoint : public USceneComponent
{
	GENERATED_BODY()

public:
	USpawnPoint();

	AActor* GetSpawnedActor() const;

protected:
	virtual void BeginPlay() override;

private:
	UPROPERTY(EditDefaultsOnly, Category = "Setup")
		TSubclassOf<AActor> SpawnClass;

	UPROPERTY()
		AActor* SpawnedActor;
};
