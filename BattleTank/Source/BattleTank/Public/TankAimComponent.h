// Kief 2018

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TankAimComponent.generated.h"


UENUM()
enum class EAimState : uint8
{
	Reloading,
	Aiming,
	Locked,
	OutOfAmmo
};


class UTankTurret;
class UTankBarrel;
class AProjectile;

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class BATTLETANK_API UTankAimComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction) override;

	// Used by Player/Ai Controllers
	void AimAt(FVector TargetLocation);

	// Used in Tank Blueprint
	UFUNCTION(BlueprintCallable, Category = "Setup")
		void Initialise(UTankBarrel * BarrelToSet, UTankTurret * TurretToSet);

	// Used in Tank Input Blueprint
	UFUNCTION(BlueprintCallable, Category = "Firing")
		void Fire();

	EAimState GetAimState() const;

	UFUNCTION(BlueprintCallable, Category = "Firing")
		int32 GetAmmoRemaining() const;

protected:
	virtual void BeginPlay() override;

	UPROPERTY(BlueprintReadOnly, Category = "State")
		EAimState AimState = EAimState::Reloading;

private:
	UTankAimComponent();

	// Used by AimAt (above)
	void MoveBarrelTowards(FVector AimDirection);

	UTankBarrel* Barrel = nullptr;
	UTankTurret* Turret = nullptr;
	float LastFireTime;

	UPROPERTY(EditDefaultsOnly, Category = "Setup")
		TSubclassOf<AProjectile> ProjectileBlueprint;

	UPROPERTY(EditDefaultsOnly, Category = "Firing")
		float LaunchSpeed = 4000;

	UPROPERTY(EditDefaultsOnly, Category = "Firing")
		float ReloadTimeInSeconds = 3;

	UPROPERTY(EditDefaultsOnly, Category = "Firing")
		int32 AmmoRemaining = 999;

	bool IsBarrelLocked();

	FVector AimDirection;
};
