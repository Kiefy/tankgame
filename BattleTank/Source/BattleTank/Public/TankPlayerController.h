// Kief 2018

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "TankPlayerController.generated.h"


class UTankAimComponent;

UCLASS()
class BATTLETANK_API ATankPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	virtual void Tick(float DeltaTime) override;

protected:
	virtual void BeginPlay() override;
	virtual void SetPawn(APawn* InPawn) override;

	// Defined in Blueprint
	UFUNCTION(BlueprintImplementableEvent, Category = "Setup")
		void FoundAimingComponent(UTankAimComponent* AimComponentRef);

private:
	void AimThroughCrosshair() const;
	bool GetAimDirection(OUT FVector& AimDirection) const;
	bool GetSightRayHitLocation(OUT FVector& AimTargetLocation) const;
	bool AimTrace(FVector AimDirection, OUT FHitResult& Hit) const;

	UFUNCTION()
		void OnDeath();

	UPROPERTY(EditDefaultsOnly)
		FVector2D CrosshairScreenPercentNormal = FVector2D(0.5f, 0.3f);

	UPROPERTY(EditDefaultsOnly)
		float AimRange = 1000000.f;

};
